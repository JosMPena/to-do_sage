# TO-DO list SAGE

## Install

Just clone or download the repository to your local environment
```
git@gitlab.com:JosMPena/to-do_sage.git
```

Navigate to the created folder

`cd to-do_sage/`

Install and configure the database

`rails db:setup`

Install the required dependencies

`bundle install`

## Using the app

Launch the app in the local environment

`rails server`

and navigate to the app url in your favorite browser

 `localhost:3000`

## Tests

Run all tests 

`rspec spec`