require 'rails_helper'

RSpec.describe TasksController, type: :controller do
  render_views
  let(:valid_attributes) do
    { title: 'Test task', description: 'A test task description' }
  end
  let(:invalid_attributes) do
    { title: nil, description: nil }
  end
  let(:user) { create :user }

  before(:each) do
    @request.env['devise.mapping'] = Devise.mappings[:user]
    sign_in user
  end

  describe 'GET #index' do
    it 'displays all created tasks on index' do
      task = user.tasks.create! valid_attributes
      get :index
      expect(response.body).to match(task.title)
      expect(response.body).to match(task.description)
    end
  end

  describe 'GET #new' do
    it 'assigns a new task as @task' do
      get :new
      expect(assigns(:task)).to be_a_new(Task)
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Task' do
        expect do
          post :create, params: { task: valid_attributes }
        end.to change(Task, :count).by(1)
      end

      it 'assigns a newly created task as @task' do
        post :create, params: { task: valid_attributes }
        expect(assigns(:task)).to be_a(Task)
        expect(assigns(:task)).to be_persisted
      end

      it 'redirects to the tasks index' do
        post :create, params: { task: valid_attributes }
        expect(response).to redirect_to(tasks_path)
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved task as @task' do
        post :create, params: { task: invalid_attributes }
        expect(assigns(:task)).to be_a_new(Task)
      end

      it "re-renders the 'new' template" do
        post :create, params: { task: invalid_attributes }
        expect(response).to render_template('new')
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested task' do
      task = user.tasks.create! valid_attributes
      expect do
        delete :destroy, params: { id: task.to_param }
      end.to change(Task, :count).by(-1)
    end
  end
end
