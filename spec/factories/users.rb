FactoryGirl.define do
  factory :user do
    email 'jose@example.com'
    password 'password'
    password_confirmation 'password'
  end
end
